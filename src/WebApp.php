<?php

// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// * Copyright 2014 The Herosphp Authors. All rights reserved.
// * Use of this source code is governed by a MIT-style license
// * that can be found in the LICENSE file.
// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

declare(strict_types=1);

namespace herosphp;

use FastRoute\Dispatcher;
use herosphp\annotation\AnnotationParser;
use herosphp\core\BeanContainer;
use herosphp\core\Config;
use herosphp\core\HttpRequest;
use herosphp\core\HttpResponse;
use herosphp\core\Router;
use herosphp\exception\BaseExceptionHandler;
use herosphp\exception\RouterException;
use herosphp\utils\Logger;
use Throwable;
use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http;
use Workerman\Worker;

require __DIR__ . DIRECTORY_SEPARATOR . 'constants.php';

/**
 * WebApp main program
 *
 * @author RockYang<yangjian102621@gmail.com>
 */
class WebApp
{
    public static HttpRequest $request;

    public static TcpConnection $connection;

    // request controller
    public static ?object $targetController = null;

    // request controller method
    public static ?string $targetMethod = null;

    protected static Dispatcher $_dispatcher;

    // app config
    protected static array $_config = [
        'listen' => 'http://0.0.0.0:2345',
        'context' => [],
        'count' => 4,
        'reloadable' => true,
        // https://www.workerman.net/doc/workerman/worker/reuse-port.html
        'reusePort' => true,
        //options settings
        'user' => '',
        'group' => '',
        'stdoutFile' => '',
        'pidFile' => '',
        'logFile' => '',
        //connections
        'defaultMaxSendBufferSize' => 1048576, //workerman default value
        'defaultMaxPackageSize' => 10485760, //workerman default value

    ];

    public static function run(): void
    {
        if (version_compare(PHP_VERSION, '8.1.0', '<')) {
            GF::printWarning('require PHP > 8.1.0 !');
            exit(0);
        }

        // run ONLY for web mode
        if (!defined('RUN_WEB_MODE')) {
            GF::printError('Error: Access only for web sapi.');
            exit(0);
        }

        // loading app configs
        $config = Config::get('app');
        if (!empty($config)) {
            static::$_config = array_merge(static::$_config, $config['server']);
        }

        // set timezone
        date_default_timezone_set($config['timezone'] ?? 'Asia/Shanghai');
        // set error report level
        error_reporting($config['error_reporting'] ?? E_ALL);

        static::startServer();
    }

    /** @noinspection PhpObjectFieldsAreOnlyWrittenInspection */
    public static function startServer(): void
    {
        // create a http worker
        $worker = new Worker(static::$_config['listen'], static::$_config['context']);

        // worker property
        $worker->name = static::$_config['name'];
        $worker->count = static::$_config['count'];
        $worker->reloadable = static::$_config['reloadable'];
        $worker->reusePort = static::$_config['reusePort'];
        // 启用 SSL 传输协议，开启 HTTPS 访问
        if (isset(static::$_config['context']['ssl'])) {
            $worker->transport = 'ssl';
        }

        // option settings
        static::$_config['user'] && $worker->user = static::$_config['user'];
        static::$_config['group'] && $worker->user = static::$_config['group'];
        static::$_config['stdoutFile'] && Worker::$stdoutFile = static::$_config['stdoutFile'];
        static::$_config['pidFile'] && Worker::$stdoutFile = static::$_config['pidFile'];
        static::$_config['logFile'] && Worker::$stdoutFile = static::$_config['logFile'];
        TcpConnection::$defaultMaxPackageSize = static::$_config['defaultMaxPackageSize'];
        TcpConnection::$defaultMaxSendBufferSize = static::$_config['defaultMaxSendBufferSize'];

        $worker->onWorkerStart = static function ($w) {
            static::onWorkerStart($w);
        };

        // http request
        Http::requestClass(HttpRequest::class);
        $worker->onMessage = static function (TcpConnection $connection, HttpRequest $request) {
            static::onMessage($connection, $request);
        };
    }

    /**
     * @throws \ReflectionException
     */
    public static function onWorkerStart(Worker $worker): void
    {
        // scan the class file and init the router info
        AnnotationParser::run(APP_PATH, 'app\\');
        static::$_dispatcher = Router::getDispatcher();
    }

    public static function onMessage(TcpConnection $connection, HttpRequest $request): void
    {
        try {
            $routeInfo = static::$_dispatcher->dispatch($request->method(), $request->path());
            static::$request = $request;
            static::$connection = $connection;
            switch ($routeInfo[0]) {
                case Dispatcher::NOT_FOUND:
                    $file = static::getPublicFile($request->path());
                    if ($file === '') {
                        $connection->send(GF::response(code: 404, body: 'Page not found.'));
                    } else {
                        if (static::notModifiedSince($file)) {
                            $connection->send((new HttpResponse(304)));
                        } else {
                            $connection->send((new HttpResponse())->withFile($file));
                        }
                    }
                    break;
                case Dispatcher::METHOD_NOT_ALLOWED:
                    $connection->send(GF::response(code: 405, body: 'Method not allowed.'));
                    break;
                case Dispatcher::FOUND:
                    $handler = $routeInfo[1];
                    $vars = $routeInfo[2];

                    // sort middlewares
                    $middlewares = static::sortMiddlewares($handler['obj']);

                    // target class
                    static::$targetController = $handler['obj'];
                    static::$targetMethod = $handler['method'];

                    $callback = GF::pipeline($middlewares, static function ($request) use ($handler, $vars) {
                        // inject request object
                        // @Note: 路径参数的相对位置应该和方法参数的相对位置保持一致
                        // 1. Controller methods can have no parameters.
                        // 2. The first parameter must be HttpRequest object.
                        // 3. Method parameters should keep the same order of the route path vars
                        if (in_array(HttpRequest::class, $handler['params_type'])) {
                            array_unshift($vars, $request);
                        }

                        if (method_exists($handler['obj'], '__init')) {
                            $handler['obj']->__init();
                        }
                        return call_user_func_array([$handler['obj'], $handler['method']], $vars);
                    });

                    $connection->send(GF::response(body: $callback($request)));
                    break;
                default:
                    throw new RouterException("router parse error for {$request->path()}");
            }

            // catch and handle the exception
        } catch (Throwable $e) {
            $connection->send(GF::response(body: static::exceptionResponse($e, $request)));
        }
    }

    // get the path for public static files
    public static function getPublicFile(string $path): string
    {
        $file = PUBLIC_PATH . $path;
        if (str_contains($file, '../')) {
            $file = str_replace('../', '', $file);
        }
        if (!is_file($file)) {
            return '';
        }
        return $file;
    }

    /**
     * @param $controllerObj
     * @return array
     */
    protected static function sortMiddlewares($controllerObj): array
    {
        $middlewares = Config::get(name: 'middleware', default: []);
        if (property_exists($controllerObj, 'middlewares')) {
            $middlewares = array_merge($middlewares, $controllerObj->middlewares);
        }
        return $middlewares;
    }

    /**
     * @param string $file
     * @return bool
     */
    public static function notModifiedSince(string $file): bool
    {
        $ifModifiedSince = self::$request->header('if-modified-since');
        if ($ifModifiedSince === null || !($mtime = \filemtime($file))) {
            return false;
        }
        return $ifModifiedSince === \gmdate('D, d M Y H:i:s', $mtime) . ' GMT';
    }

    /**
     * 统一异常处理
     * @param Throwable $e
     * @param HttpRequest $request
     * @return HttpResponse
     */
    protected static function exceptionResponse(Throwable $e, HttpRequest $request): mixed
    {
        try {
            //check exception exist
            if (BeanContainer::exist('app\\exception\\ExceptionHandler')) {
                $exceptionHandler = BeanContainer::get('app\\exception\\ExceptionHandler');
            } else {
                $exceptionHandler = BeanContainer::make(BaseExceptionHandler::class);
            }
            $exceptionHandler->report($e);
            return $exceptionHandler->render($request, $e);
        } catch (Throwable $e) {
            if (GF::getAppConfig('debug')) {
                Logger::error($e->getMessage());
            }
            return GF::response(code: 500, body: 'Oops, it seems something went wrong.');
        }
    }
}
