<?php

// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// * Copyright 2014 The Herosphp Authors. All rights reserved.
// * Use of this source code is governed by a MIT-style license
// * that can be found in the LICENSE file.
// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

declare(strict_types=1);

namespace herosphp\exception;

use herosphp\session\SessionError;

/**
 * @author RockYang<yangjian102621@gmail.com>
 */
class SessionException extends HeroException
{
    public function __construct(SessionError $error)
    {
        parent::__construct($error->getMessage(), $error->value);
    }
}
