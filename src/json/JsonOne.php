<?php

// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// * Copyright 2014 The Herosphp Authors. All rights reserved.
// * Use of this source code is governed by a MIT-style license
// * that can be found in the LICENSE file.
// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

declare(strict_types=1);

namespace herosphp\json;

use herosphp\utils\StringUtil;

class JsonOne implements Jsonable
{
    public function __construct(public int $code, public string $message, public mixed $data = null)
    {
    }

    public static function create(int $code, string $message, mixed $data = null): self
    {
        return new self($code, $message, $data);
    }

    public static function success(mixed $data, int $code = 0): self
    {
        return self::create($code, "", $data);
    }

    public static function fail(string $message, int $code = 1): self
    {
        return self::create($code, $message);
    }

    public function toJson(): string
    {
        if (!$this->data) {
            // only return code and message,like weChat
            return StringUtil::jsonEncode([
                'code' => $this->code,
                'message' => $this->message,
            ]);
        }
        return StringUtil::jsonEncode([
            'code' => $this->code,
            'message' => $this->message,
            'data' => $this->data
        ]);
    }
}
