<?php

namespace herosphp\session;

use \Workerman\Protocols\Http\Session\RedisSessionHandler as WorkermanRedisSessionHandler;

/**
 * Class RedisSessionHandler
 * @package Workerman\Protocols\Http\Session
 */
class RedisSessionHandler extends WorkermanRedisSessionHandler
{
    /**
     * {@inheritdoc}
     */
    public function write($session_id, $session_data)
    {
        return true === $this->_redis->setex($session_id, Session::$lifetime, $session_data);
    }

    /**
     * {@inheritdoc}
     */
    public function updateTimestamp($id, $data = '')
    {
        return true === $this->_redis->expire($id, Session::$lifetime);
    }
}
